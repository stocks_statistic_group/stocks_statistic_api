package net.proselyte.stocks_statistic_api.utils;

import net.proselyte.stocks_statistic_api.dto.QuoteClientDto;
import net.proselyte.stocks_statistic_api.dto.SymbolClientDto;
import net.proselyte.stocks_statistic_api.dto.SymbolNotificationDto;
import net.proselyte.stocks_statistic_api.entity.Quote;
import net.proselyte.stocks_statistic_api.entity.Symbol;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import java.util.List;

@org.mapstruct.Mapper
public interface MapperDto {

    @BeanMapping(ignoreByDefault = true)
    @Mappings({
            @Mapping(target = "symbolCode", source = "dto.symbol"),
            @Mapping(target = "name", source = "dto.name"),
            @Mapping(target = "enabled", source = "dto.enabled")
    })
    Symbol symbolClientDtoToSymbol(SymbolClientDto dto);

    List<Symbol> symbolClientDtoToSymbol(List<SymbolClientDto> dtos);

    @BeanMapping(ignoreByDefault = true)
    @Mappings({
            @Mapping(target = "symbolCode", source = "dto.symbol"),
            @Mapping(target = "previousVolume", source = "dto.previousVolume"),
            @Mapping(target = "volume", source = "dto.volume"),
            @Mapping(target = "latestPrice", source = "dto.latestPrice")
    })
    Quote quoteClientDtoToQuote(QuoteClientDto dto);

    List<Quote> quoteClientDtoToQuote(List<QuoteClientDto> dtos);

    @Mappings({
            @Mapping(target = "symbolCode", source = "symbol.symbolCode"),
            @Mapping(target = "name", source = "symbol.name")
    })
    SymbolNotificationDto SymbolToSymbolNotificationDto(Symbol symbol);

    List<SymbolNotificationDto> SymbolToSymbolNotificationDto(List<Symbol> symbols);
}
