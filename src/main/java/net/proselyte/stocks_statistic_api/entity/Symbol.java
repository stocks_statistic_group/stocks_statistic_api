package net.proselyte.stocks_statistic_api.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "SYMBOLS")
@Getter
@Setter
@NoArgsConstructor
public class Symbol {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "symbol_code")
    private String symbolCode;
    @Column(name = "name")
    private String name;
    @Column(name = "is_enabled")
    private boolean isEnabled;

    @OneToMany(mappedBy = "symbolObject", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Quote> quotes;
}
