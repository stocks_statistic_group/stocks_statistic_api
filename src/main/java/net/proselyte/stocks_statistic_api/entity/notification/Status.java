package net.proselyte.stocks_statistic_api.entity.notification;

public enum Status {
    NEW, COMPLETED
}
