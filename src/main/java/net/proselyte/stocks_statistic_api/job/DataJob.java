package net.proselyte.stocks_statistic_api.job;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.proselyte.stocks_statistic_api.client.IexApiClient;
import net.proselyte.stocks_statistic_api.dto.SymbolClientDto;
import net.proselyte.stocks_statistic_api.entity.Symbol;
import net.proselyte.stocks_statistic_api.utils.MapperDto;
import net.proselyte.stocks_statistic_api.service.QuoteService;
import net.proselyte.stocks_statistic_api.service.ReportService;
import net.proselyte.stocks_statistic_api.service.SymbolService;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Slf4j
@Component
@EnableScheduling
@RequiredArgsConstructor
public class DataJob {

    private final IexApiClient iexApiClient;
    private final QuoteService quoteService;
    private final SymbolService symbolService;
    private final ReportService reportService;
    private final MapperDto mapperDto;
    @Value("${titleOfTop5SymbolsWithMaxVolumeOrPreviousVolume}")
    private String titleOfTop5SymbolsWithMaxVolumeOrPreviousVolume;
    @Value("${titleOfTop5SymbolsGreatestChangeLatestPrice}")
    private String titleOfTop5SymbolsGreatestChangeLatestPrice;
    @Value("${partition.inputSymbolsList.size}")
    private int partitionSize;

    @Scheduled(cron = "${processData.scheduled.cron}")
    public void processData() throws ExecutionException, InterruptedException {
        List<SymbolClientDto> symbolClientDtos;
        try {
            symbolClientDtos = iexApiClient.getSymbols();
            List<Symbol> symbols = mapperDto.symbolClientDtoToSymbol(symbolClientDtos);
            symbols = symbolService.checkStatus(symbols);
            List<List<Symbol>> subSets = ListUtils.partition(symbols, partitionSize);
            for (List<Symbol> subSymbols : subSets) {
                subSymbols = quoteService.addQuotes(subSymbols);
                symbolService.updateSymbols(subSymbols);
            }
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
        }
    }

    @Scheduled(cron = "${showReport.scheduled.cron}")
    void showReport() {
        log.info("--------------Start------------");
        List<String> listString1 = reportService.getTop5SymbolsWithMaxVolumeOrPreviousVolumeToListString();
        List<String> listString2 = reportService.getTop5SymbolsGreatestChangeLatestPriceToListString();
        log.info(titleOfTop5SymbolsWithMaxVolumeOrPreviousVolume);
        listString1.forEach(log::info);
        log.info(titleOfTop5SymbolsGreatestChangeLatestPrice);
        listString2.forEach(log::info);
        log.info("---------------End-------------");
    }
}
