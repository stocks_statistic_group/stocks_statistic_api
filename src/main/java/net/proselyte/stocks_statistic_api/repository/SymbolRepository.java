package net.proselyte.stocks_statistic_api.repository;

import net.proselyte.stocks_statistic_api.entity.Symbol;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface SymbolRepository extends CrudRepository<Symbol, Long> {

    Optional<Symbol> findFirstBySymbolCode(String symbolCode);

    Iterable<Symbol> findAllByIsEnabledTrue();

    @Query(value = "select sym.id, sym.is_enabled, sym.name, sym.symbol_code from (select s.id, is_enabled, name, s.symbol_code, greatest(max(q.volume), max(q.previous_volume)) as max_volume from symbols s " +
            "join quotes q on(s.id = q.symbol_id) where is_enabled = true " +
            "group by s.id order by  max_volume desc nulls last limit 5) sym", nativeQuery = true)
    List<Symbol> findFirst5MaxVolumeOrPreviousVolume();

    @Transactional
    @Modifying
    @Query("update Symbol s set s.isEnabled = :isEnabled where s.id = :id")
    void updateEnabledById(@Param(value = "id") Long id, @Param(value = "isEnabled") boolean isEnabled);
}

