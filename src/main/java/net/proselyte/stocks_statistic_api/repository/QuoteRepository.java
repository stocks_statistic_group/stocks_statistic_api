package net.proselyte.stocks_statistic_api.repository;

import net.proselyte.stocks_statistic_api.entity.Quote;
import org.springframework.data.repository.CrudRepository;
import java.util.List;
import java.util.Optional;

public interface QuoteRepository extends CrudRepository<Quote, Long> {

    Optional<Quote> findFirstBySymbolCodeOrderByIdDesc(String symbolCode);

    List<Quote> findFirst2BySymbolCodeOrderByIdDesc(String symbolCode);
}

