package net.proselyte.stocks_statistic_api.dto;

import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;

@Getter
@Setter
public class SymbolReport {

    private String symbolCode;
    private String name;
    private BigDecimal volume;
    private BigDecimal changeLast2LatestPricesPercent;
}
