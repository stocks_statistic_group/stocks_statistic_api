package net.proselyte.stocks_statistic_api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SymbolNotificationDto {

    private String symbolCode;
    private String name;
}
