package net.proselyte.stocks_statistic_api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;

@Getter
@Setter
public class QuoteClientDto {

    private String symbol;
    private BigDecimal previousVolume;
    private BigDecimal volume;
    private BigDecimal latestPrice;
    private String companyName;
    private String primaryExchange;
    private String calculationPrice;
    private String open;
    private String openTime;
    private String openSource;
    private String close;
    private String closeTime;
    private String closeSource;
    private String high;
    private String highTime;
    private String highSource;
    private String low;
    private String lowTime;
    private String lowSource;
    private String latestSource;
    private String latestTime;
    private String latestUpdate;
    private String latestVolume;
    private String iexRealtimePrice;
    private String iexRealtimeSize;
    private String iexLastUpdated;
    private String delayedPrice;
    private String delayedPriceTime;
    private String oddLotDelayedPrice;
    private String oddLotDelayedPriceTime;
    private String extendedPrice;
    private String extendedChange;
    private String extendedChangePercent;
    private String extendedPriceTime;
    private String previousClose;
    private String change;
    private String changePercent;
    private String iexMarketPercent;
    private String iexVolume;
    private String avgTotalVolume;
    private String iexBidPrice;
    private String iexBidSize;
    private String iexAskPrice;
    private String iexAskSize;
    private String iexOpen;
    private String iexOpenTime;
    private String iexClose;
    private String iexCloseTime;
    private String marketCap;
    private String peRatio;
    private String week52High;
    private String week52Low;
    private String ytdChange;
    private String lastTradeTime;
    private String currency;
    @JsonProperty("isUSMarketOpen")
    private boolean isUSMarketOpen;
}
