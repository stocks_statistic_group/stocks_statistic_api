package net.proselyte.stocks_statistic_api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SymbolClientDto {

    private String symbol;
    private String name;
    @JsonProperty("isEnabled")
    private boolean isEnabled;
    private String exchange;
    private String date;
    private String type;
    private String region;
    private String currency;
    private String iexId;
    private String figi;
    private String cik;
}
