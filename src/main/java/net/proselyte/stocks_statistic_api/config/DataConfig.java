package net.proselyte.stocks_statistic_api.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
@EnableJpaRepositories("net.proselyte.stocks_statistic_api.repository")
public class DataConfig {

    @Value("${executorService.numberOfThreads}")
    private int nThreads;

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public ExecutorService getExecutorService() {
        return Executors.newFixedThreadPool(nThreads);
    }
}
