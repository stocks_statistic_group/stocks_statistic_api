package net.proselyte.stocks_statistic_api.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import net.proselyte.stocks_statistic_api.dto.QuoteClientDto;
import net.proselyte.stocks_statistic_api.dto.SymbolClientDto;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.util.Arrays;
import java.util.List;

@Component
@RequiredArgsConstructor
public class IexApiClient {

    private final RestTemplate restTemplate;
    private final RequestPath requestPath;

    public List<SymbolClientDto> getSymbols() throws JsonProcessingException {
        String response = sendRequest(requestPath.getSymbols());
        SymbolClientDto[] symbolsDtosArray = getObjectMapper().readValue(response, SymbolClientDto[].class);
        return Arrays.asList(symbolsDtosArray);
    }

    public QuoteClientDto getQuoteBySymbolCode(String symbolCode) throws JsonProcessingException {
        String response = sendRequest(requestPath.getQuotesBySymbolCode(symbolCode));
        return getObjectMapper().readValue(response, QuoteClientDto.class);
    }

    private ObjectMapper getObjectMapper() {
        return new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    private String sendRequest(String path) {
        return restTemplate
                .getForEntity(path + "?token=" + requestPath.getToken(), String.class)
                .getBody();
    }
}
