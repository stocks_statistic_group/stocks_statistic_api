package net.proselyte.stocks_statistic_api.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RequestPath {

    private static final String PATH_SYMBOLS = "/stable/ref-data/symbols";
    private static final String PATH_QUOTES = "/stable/stock/%s/quote";
    @Value("${client.token}")
    private String token;
    @Value("${client.baseUrl}")
    private String baseUrl;

    public String getToken() {
        return token;
    }

    public String getSymbols() {
        return baseUrl.concat(PATH_SYMBOLS);
    }

    public String getQuotesBySymbolCode(String symbolCode) {
        return baseUrl.concat(String.format(PATH_QUOTES, symbolCode));
    }
}
