package net.proselyte.stocks_statistic_api.aspects;

import lombok.RequiredArgsConstructor;
import net.proselyte.stocks_statistic_api.service.NotificationService;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
@RequiredArgsConstructor
public class ExceptionHandlingAspect {

    private final NotificationService notificationService;

    @Pointcut(value = "execution(public * net.proselyte.stocks_statistic_api..*(..))")
    private void anyPublicMethod() {
    }

    @AfterThrowing(pointcut = "anyPublicMethod()", throwing = "exception")
    public void afterThrowingPublicMethodAdvice(Throwable exception) {
        notificationService.thrownException(exception);
    }
}
