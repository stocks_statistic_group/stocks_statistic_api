package net.proselyte.stocks_statistic_api.service;

import lombok.RequiredArgsConstructor;
import net.proselyte.stocks_statistic_api.entity.Quote;
import net.proselyte.stocks_statistic_api.entity.Symbol;
import net.proselyte.stocks_statistic_api.repository.SymbolRepository;
import org.springframework.stereotype.Service;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SymbolService {

    private final SymbolRepository symbolRepository;
    private final QuoteService quoteService;
    private final NotificationService notificationService;

    public Iterable<Symbol> findAllByIsEnabledTrue() {
        return symbolRepository.findAllByIsEnabledTrue();
    }

    public List<Symbol> findFirst5MaxVolume() {
        return symbolRepository.findFirst5MaxVolumeOrPreviousVolume();
    }

    public void updateSymbols(List<Symbol> symbols) {
        List<Symbol> newSymbols = symbols.stream()
                .filter(s -> symbolRepository.findFirstBySymbolCode(s.getSymbolCode()).isEmpty())
                .collect(Collectors.toList());
        symbolRepository.saveAll(newSymbols);
        notificationService.addedNewSymbols(newSymbols);
        List<Quote> newQuotes = symbols.stream()
                .filter(s -> symbolRepository.findFirstBySymbolCode(s.getSymbolCode()).isPresent())
                .flatMap(s -> s.getQuotes().stream())
                .filter(q -> {
                    if (quoteService.findLastQuoteBySymbolCode(q.getSymbolCode()).isPresent()) {
                        return !quoteService.findLastQuoteBySymbolCode(q.getSymbolCode()).get().equals(q);
                    } else {
                        return true;
                    }
                })
                .collect(Collectors.toList());
        List<String> symbolCodes = newQuotes.stream().map(Quote::getSymbolCode).collect(Collectors.toList());
        Map<String, Symbol> symbolCodeAndSymbolFromDB = getSymbolsFromDataBaseByListOfSymbolCodes(symbolCodes);
        List<Quote> newQuotesWithSymbols = addSymbolsToQuotes(symbolCodeAndSymbolFromDB, newQuotes);
        quoteService.saveAll(newQuotesWithSymbols);
    }

    private List<Quote> addSymbolsToQuotes(Map<String, Symbol> symbolCodeAndSymbolFromDB, List<Quote> quotes) {
        quotes.forEach(q -> q.setSymbolObject(symbolCodeAndSymbolFromDB.get(q.getSymbolCode())));
        return quotes;
    }

    private Map<String, Symbol> getSymbolsFromDataBaseByListOfSymbolCodes(List<String> symbolCodes) {
        Map<String, Symbol> map = new HashMap<>();
        symbolCodes.stream()
                .filter(s -> symbolRepository.findFirstBySymbolCode(s).isPresent())
                .forEach(s -> {
                    if (symbolRepository.findFirstBySymbolCode(s).isPresent()) {
                        map.put(s, symbolRepository.findFirstBySymbolCode(s).get());
                    }
                });
        return map;
    }

    public List<Symbol> checkStatus(List<Symbol> symbols) {
        List<String> symbolCodes = symbols.stream().map(Symbol::getSymbolCode).collect(Collectors.toList());
        Map<String, Symbol> symbolsFromDataBase = getSymbolsFromDataBaseByListOfSymbolCodes(symbolCodes);
        Collection<Symbol> values = symbolsFromDataBase.values();
        values.forEach(s -> symbolRepository.updateEnabledById(s.getId(), s.isEnabled()));
        return symbols.stream().filter(Symbol::isEnabled).collect(Collectors.toList());
    }
}

