package net.proselyte.stocks_statistic_api.service;

import lombok.RequiredArgsConstructor;
import net.proselyte.stocks_statistic_api.client.IexApiClient;
import net.proselyte.stocks_statistic_api.entity.Quote;
import net.proselyte.stocks_statistic_api.entity.Symbol;
import net.proselyte.stocks_statistic_api.repository.QuoteRepository;
import net.proselyte.stocks_statistic_api.utils.MapperDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
public class QuoteService {

    private final QuoteRepository quoteRepository;
    private final ExecutorService executorService;
    private final IexApiClient iexApiClient;
    private final MapperDto mapperDto;
    @Value("${executorService.awaitTermination.timeout}")
    private Long timeout;

    public Optional<Quote> findLastQuoteBySymbolCode(String symbolCode) {
        return quoteRepository.findFirstBySymbolCodeOrderByIdDesc(symbolCode);
    }

    public List<Quote> findLast2QuotesBySymbolCode(String symbolCode) {
        return quoteRepository.findFirst2BySymbolCodeOrderByIdDesc(symbolCode);
    }

    public void saveAll(List<Quote> quotes) {
        quoteRepository.saveAll(quotes);
    }

    public List<Symbol> addQuotes(List<Symbol> symbols) throws ExecutionException, InterruptedException {
        List<Symbol> symbolsWithQuotes = new ArrayList<>();
        for (Symbol symbol : symbols) {
            Future<Symbol> futures = executorService
                    .submit(() -> {
                        var quoteClientDto = iexApiClient.getQuoteBySymbolCode(symbol.getSymbolCode());
                        var quote = mapperDto.quoteClientDtoToQuote(quoteClientDto);
                        quote.setSymbolObject(symbol);
                        symbol.setQuotes(Collections.singletonList(quote));
                        return symbol;
                    });
            symbolsWithQuotes.add(futures.get());
        }
        executorService.awaitTermination(timeout, TimeUnit.MILLISECONDS);
        return symbolsWithQuotes;
    }
}
