package net.proselyte.stocks_statistic_api.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.proselyte.stocks_statistic_api.entity.Symbol;
import net.proselyte.stocks_statistic_api.entity.notification.Notification;
import net.proselyte.stocks_statistic_api.entity.notification.Status;
import net.proselyte.stocks_statistic_api.entity.notification.Type;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class NotificationService {

    private final QueueMessagingTemplate queueMessagingTemplate;
    private final MapperDto mapperDto;
    @Value("${cloud.aws.end-point.uri}")
    private String endpoint;
    @Value("${service.name}")
    private String serviceName;

    public void addedNewSymbols(List<Symbol> newSymbols) {
        if (newSymbols != null) {
            newSymbols.forEach(symbol -> queueMessagingTemplate
                    .convertAndSend(endpoint,
                            new Notification(Status.NEW, Type.INFO, LocalDateTime.now(), serviceName,
                                    mapperDto.SymbolToSymbolNotificationDto(symbol))));
        }
    }

    public void thrownException(Throwable exception) {
        try {
            queueMessagingTemplate
                    .convertAndSend(endpoint,
                            new Notification(Status.NEW, Type.ERROR, LocalDateTime.now(), serviceName,
                                    exception.getStackTrace()));
        } catch (Exception e) {
            log.error("NotificationService.thrownException() throw exception:" + e.getMessage());
        }
    }
}
