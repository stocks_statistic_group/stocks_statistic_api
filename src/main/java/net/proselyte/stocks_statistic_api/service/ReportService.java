package net.proselyte.stocks_statistic_api.service;

import lombok.RequiredArgsConstructor;
import net.proselyte.stocks_statistic_api.dto.SymbolReport;
import net.proselyte.stocks_statistic_api.entity.Quote;
import net.proselyte.stocks_statistic_api.entity.Symbol;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class ReportService {

    @Value("${calculations.scale}")
    private int scale;
    private final SymbolService symbolService;
    private final QuoteService quoteService;

    public List<String> getTop5SymbolsWithMaxVolumeOrPreviousVolumeToListString() {
        return getTop5SymbolsWithMaxVolumeOrPreviousVolume().stream()
                .filter(Objects::nonNull)
                .map(r -> r.getSymbolCode().concat(" ").concat(r.getName())
                        .concat(" ").concat(r.getVolume().toString()))
                .collect(Collectors.toList());
    }

    public List<String> getTop5SymbolsGreatestChangeLatestPriceToListString() {
        return getTop5SymbolsGreatestChangeLatestPrice().stream()
                .filter(Objects::nonNull)
                .map(r -> r.getSymbolCode().concat(" ").concat(r.getName())
                        .concat(" ").concat(r.getChangeLast2LatestPricesPercent().toString()).concat(" %"))
                .collect(Collectors.toList());
    }

    private List<SymbolReport> getTop5SymbolsWithMaxVolumeOrPreviousVolume() {
        Iterable<Symbol> iterable = symbolService.findFirst5MaxVolume();
        return StreamSupport.stream(iterable.spliterator(), false)
                .map(this::symbolToReport)
                .sorted((symbolReport1, symbolReport2) ->
                        Objects.requireNonNullElse(symbolReport2.getVolume(), BigDecimal.ZERO)
                                .compareTo(Objects.requireNonNullElse(symbolReport1.getVolume(), BigDecimal.ZERO))
                )
                .limit(5)
                .collect(Collectors.toList());
    }

    private List<SymbolReport> getTop5SymbolsGreatestChangeLatestPrice() {
        Iterable<Symbol> iterable = symbolService.findAllByIsEnabledTrue();
        return StreamSupport.stream(iterable.spliterator(), false)
                .map(this::symbolToReport)
                .sorted(Comparator.comparing(SymbolReport::getChangeLast2LatestPricesPercent).reversed())
                .limit(5)
                .collect(Collectors.toList());
    }

    public SymbolReport symbolToReport(Symbol symbol) {
        var symbolReport = new SymbolReport();
        symbolReport.setSymbolCode(symbol.getSymbolCode());
        symbolReport.setName(symbol.getName());
        symbolReport.setVolume(selectVolumeOrPreviousVolume(symbol.getSymbolCode()));
        symbolReport.setChangeLast2LatestPricesPercent(getChangeLast2LatestPricesPercent(symbol.getSymbolCode()));
        return symbolReport;
    }

    private BigDecimal selectVolumeOrPreviousVolume(String symbolCode) {
        BigDecimal volume = BigDecimal.ZERO;
        Optional<Quote> lastQuoteBySymbol = quoteService.findLastQuoteBySymbolCode(symbolCode);
        if (lastQuoteBySymbol.isPresent()) {
            var quote = lastQuoteBySymbol.get();
            if (quote.getVolume() != null) {
                volume = quote.getVolume();
            } else if (quote.getPreviousVolume() != null) {
                volume = quote.getPreviousVolume();
            }
        }
        return volume;
    }

    private BigDecimal getChangeLast2LatestPricesPercent(String symbolCode) {
        BigDecimal changePercent = BigDecimal.ZERO;
        List<Quote> last2Quotes = quoteService.findLast2QuotesBySymbolCode(symbolCode);
        if (last2Quotes.size() == 2) {
            var lastQuote = last2Quotes.get(0);
            var quote2 = last2Quotes.get(1);
            changePercent = lastQuote.getLatestPrice()
                    .subtract(quote2.getLatestPrice())
                    .divide(quote2.getLatestPrice(), scale, RoundingMode.DOWN)
                    .multiply(BigDecimal.valueOf(100));
        }
        return changePercent;
    }
}
