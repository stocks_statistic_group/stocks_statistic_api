# stocks_statistic_api

#User settings in application.properties
access-key, secret-key, region.static, end-point.uri


OS: Amazon Linux 2 AMI (HVM), SSD Volume Type


chmod 400 statistic-api.pem
ssh -i "statistic-api.pem" ec2-user@ec2-X-X-X-X.eu-central-1.compute.amazonaws.com

sudo amazon-linux-extras install java-openjdk11


sudo tee /etc/yum.repos.d/pgdg.repo<<EOF
[pgdg13]
name=PostgreSQL 13 for RHEL/CentOS 7 - x86_64
baseurl=https://download.postgresql.org/pub/repos/yum/13/redhat/rhel-7-x86_64
enabled=1
gpgcheck=0
EOF

sudo yum -y update

sudo yum install postgresql13 postgresql13-server

sudo /usr/pgsql-13/bin/postgresql-13-setup initdb

sudo systemctl start postgresql-13
sudo systemctl enable postgresql-13

sudo systemctl status postgresql-13

sudo passwd postgres

su - postgres
psql -c "ALTER USER postgres WITH PASSWORD 'your-password';"
psql -c "CREATE DATABASE stocks_statistic_api;"

chmod 400 statistic-api.pem
scp -i "/Users/kurowskill/Downloads/statistic-api.pem" -r "/Users/kurowskill/Documents/IDEArepository/stocks-statistic-API/target/stocks-statistic-API-0.0.1-SNAPSHOT.jar" ec2-user@ec2-X-X-X-X.eu-central-1.compute.amazonaws.com:~/

su - postgres
psql
\l
\c stocks_statistic_api
\dt


----
Install Docker and Docker-compose
sudo amazon-linux-extras install docker
sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

sudo systemctl start docker


docker save busybox > busybox.tar
sudo service docker start
docker load --input fedora.tar
