FROM openjdk:11

RUN mkdir -p /usr/local/src
COPY . /usr/local/src
WORKDIR /usr/local/src
RUN ./mvnw package
ARG JAR_FILE=/usr/local/src/target/*SNAPSHOT.jar
RUN mkdir -p /usr/local/stocks_statistic_api
RUN cp ${JAR_FILE} /usr/local/stocks_statistic_api/app.jar
RUN rm -r /usr/local/src
WORKDIR /usr/local/stocks_statistic_api

ARG DATASOURCE_URL
ARG DATASOURCE_USERNAME
ARG DATASOURCE_PASSWORD
ARG CLIENT_TOKEN
ARG CLIENT_BASEURL
ARG AWS_SQS_ACCESS_KEY
ARG AWS_SQS_SECRET_KEY
ARG AWS_SQS_REGION
ARG AWS_SQS_URI

ENV DATASOURCE_URL=${DATASOURCE_URL}
ENV DATASOURCE_USERNAME=${DATASOURCE_USERNAME}
ENV DATASOURCE_PASSWORD=${DATASOURCE_PASSWORD}
ENV CLIENT_TOKEN=${CLIENT_TOKEN}
ENV CLIENT_BASEURL=${CLIENT_BASEURL}
ENV AWS_SQS_ACCESS_KEY=${AWS_SQS_ACCESS_KEY}
ENV AWS_SQS_SECRET_KEY=${AWS_SQS_SECRET_KEY}
ENV AWS_SQS_REGION=${AWS_SQS_REGION}
ENV AWS_SQS_URI=${AWS_SQS_URI}

ENTRYPOINT java -jar app.jar \
--datasource.url=${DATASOURCE_URL} \
--datasource.username=${DATASOURCE_USERNAME} \
--datasource.password=${DATASOURCE_PASSWORD} \
--client.token=${CLIENT_TOKEN} \
--client.baseurl=${CLIENT_BASEURL} \
--aws.sqs.access-key=${AWS_SQS_ACCESS_KEY} \
--aws.sqs.secret-key=${AWS_SQS_SECRET_KEY} \
--aws.sqs.region=${AWS_SQS_REGION} \
--aws.sqs.uri=${AWS_SQS_URI}
